## About Rest Easy App

Rest Easy Hotel Management sytem the administer bookings:

- [The project was developed using the Laravel Framework](https://laravel.com).


## License

This work is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
